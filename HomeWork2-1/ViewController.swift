//
//  ViewController.swift
//  G70L2
//
//  Created by Volodymyr on 04.04.2019.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import UIKit

class ViewController: UIViewController { 

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        higestNumber()
        print("\n")
        squarAndCube()
        print("\n")
        recount(23)
        print("\n")
        totalNumberOfDivisors()
        print("\n")
        perfectNunber()
    }
    func generatedNumber(from: Int, to: Int) -> Int {
        let numA = Int.random(in: from...to)
        return numA
    }
    // Задача 0. Вывести на экран наибольшее из двух чисел
    func higestNumber() ->  Int {
        let firstNumb = generatedNumber(from: 50, to: 170)
        let secondNumb = generatedNumber(from: 23, to: 170)
        print("Это первое число: \(firstNumb)")
        print("Это второе число: \(secondNumb)")
        if firstNumb == secondNumb {
            let total = firstNumb
            print("Числа равны \(firstNumb)==\(secondNumb)")
            return total
        }
        if firstNumb < secondNumb {
            let total = secondNumb
            print("Второе число \(secondNumb) больше первого \(firstNumb)")
            return total
        }
        if firstNumb > secondNumb {
            let total = firstNumb
            print("Второе число \(secondNumb) меньше первого \(firstNumb)")
            return total
        }
        else {
            // вы нужденный элс так как xcode ругался на то что он невидет ретерна
            return firstNumb
        }
    }
    // Задача 1. Вывести на экран квадрат и куб числа
    func squarAndCube() -> (Int, Int) {
        let randomNumber = generatedNumber(from: 1, to: 7)
        var squarNumb: Int
        var cubeNumb: Int
        cubeNumb = randomNumber * randomNumber
        squarNumb = randomNumber * randomNumber * randomNumber
        print("Квадрат числа: \(randomNumber) равен: \(cubeNumb)")
        print("Куб числа: \(randomNumber) равен: \(squarNumb)")
        return (cubeNumb, squarNumb)
    }
    // Задача 2. Вывести на экран все числа до заданного и в обратном порядке до 0
    func recount(_ numB: Int) -> [Int] {
        print("Исходное число: \(numB)")
        var someArray: [Int] = []
        var standartOrder = 0
        var reversOrder = numB
        reversOrder += 1
        for _ in 0..<numB  {
            standartOrder += 1
            reversOrder -= 1
            print("Порядок увеличения: \(standartOrder) Обратный порядок: \(reversOrder)")
            someArray.append(reversOrder)
        }
       return someArray
       
    }
    // Задача 3. Подсчитать общее количество делителей числа и вывести их
    func totalNumberOfDivisors() -> Int {
        let randomNumber = generatedNumber(from: 1, to: 1000)
        var counterNumber = 0
        var displayNumber = 0
        var xNumber = 0
        print("Для этого числа: \(randomNumber) ищем делители")
        for _ in 0..<randomNumber {
            counterNumber  += 1
            displayNumber = randomNumber % counterNumber
            if displayNumber == 0 {
                xNumber += 1
                print("Это делитель: \(counterNumber)")
            }
        }
        print("Кол-во делителей:  \(xNumber)")
        return xNumber
    }
    // Задача 4. Проверить, является ли заданное число совершенным и найти их (делители)
    func perfectNunber() {
        let randomNumber = generatedNumber(from: 1, to: 10000)
        var resultNuber = 0
        var summOfDivisor = 0
        var loopNum = 0
        print("Случайное число от 1 до 10000: \(randomNumber)")
        for _ in 1...randomNumber {
            loopNum += 1
            resultNuber = randomNumber % loopNum
            if resultNuber == 0 {
                summOfDivisor += loopNum
                print("Это делитель \(loopNum)")
                if summOfDivisor != randomNumber {
                    print("Это число не совершенно: \(summOfDivisor)")
                }
                else {
                    print("Это число совершенно: \(summOfDivisor)")
                }
                break
            }
        }
    }
}
